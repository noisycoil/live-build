#!/bin/sh

## live-build(7) - System Build Scripts
## Copyright (C) 2006-2011 Daniel Baumann <daniel@debian.org>
##
## live-build comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.


set -e

# Including common functions
. "${LB_BASE:-/usr/share/live/build}"/scripts/build.sh

# Setting static variables
DESCRIPTION="$(Echo 'encrypts rootfs')"
HELP=""
USAGE="${PROGRAM} [--force]"

Arguments "${@}"

# Reading configuration files
Read_conffiles config/all config/common config/bootstrap config/chroot config/binary config/source
Set_defaults

if [ "${LB_BINARY_IMAGES}" = "virtual-hdd" ]
then
	exit 0
fi

case "${LB_ENCRYPTION}" in
	aes128|aes192|aes256)
		;;
	""|false)
		exit 0
		;;
	*)
		Echo_error "Encryption type %s not supported." "${LB_ENCRYPTION}"
		exit 1
		;;
esac

case "${LB_CHROOT_FILESYSTEM}" in
	ext2|squashfs)
		;;

	*)
		Echo_error "Encryption not yet supported on %s filesystems." "${LB_CHROOT_FILESYSTEM}"
		exit 1
		;;
esac

Echo_message "Begin encrypting root filesystem image..."

# Requiring stage file
Require_stagefile .stage/config .stage/bootstrap .stage/binary_rootfs

# Checking stage file
Check_stagefile .stage/binary_encryption

# Checking lock file
Check_lockfile .lock

# Creating lock file
Create_lockfile .lock

case "${LB_INITRAMFS}" in
	casper)
		INITFS="casper"
		;;

	live-initramfs|live-boot)
		INITFS="live"
		;;
esac

# Checking depends
Check_package chroot/usr/bin/aespipe aespipe

# Restoring cache
Restore_cache cache/packages_binary

# Installing depends
Install_package

Echo_message "Encrypting binary/%s/filesystem.%s with %s..." "${INITFS}" "${LB_CHROOT_FILESYSTEM}" "${LB_ENCRYPTION}"

if [ "${LB_BUILD_WITH_CHROOT}" = "true" ]
then
	# Moving image
	mv binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM} chroot
fi

while true
do
	echo
	echo " **************************************"
	Echo " ** Configuring encrypted filesystem **"
	echo " **************************************"
	Echo " (Passwords must be at least 20 characters long)"
	echo

	case "${LB_BUILD_WITH_CHROOT}" in
		true)
			if Chroot chroot aespipe -e ${LB_ENCRYPTION} -T \
				< chroot/filesystem.${LB_CHROOT_FILESYSTEM} \
				> chroot/filesystem.${LB_CHROOT_FILESYSTEM}.tmp
			then
				mv chroot/filesystem.${LB_CHROOT_FILESYSTEM}.tmp binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM}
				break
			fi
			;;
		false)
			if aespipe -e ${LB_ENCRYPTION} -T \
				< binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM} \
				> binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM}.tmp
			then
				mv binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM}.tmp binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM}
				break
			fi
			;;
	esac

	printf "\nThere was an error configuring encryption ... Retry? [Y/n] "
	read ANSWER

	if [ "$(echo "${ANSWER}" | cut -b1 | tr A-Z a-z)" = "n" ]
	then
		unset ANSWER
		break
	fi
done
	
# Cleanup temporary filesystems
rm -f chroot/filesystem.${LB_CHROOT_FILESYSTEM}
rm -f chroot/filesystem.${LB_CHROOT_FILESYSTEM}.tmp
rm -f binary/${INITFS}/filesystem.${LB_CHROOT_FILESYSTEM}.tmp

# Saving cache
Save_cache cache/packages_binary

# Removing depends
Remove_package

# Creating stage file
Create_stagefile .stage/binary_encryption
