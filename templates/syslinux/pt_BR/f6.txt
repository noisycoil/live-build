0fPARÂMETROS ESPECIAIS DE INICIALIZAÇÃO - HARDWARES DIVERSOS07                                    09F607

Você pode utilizar os seguintes parâmetros de inicialização no prompt
0fboot:07, em combinação com o método de inicialização (veja <09F307>).
Se você utilizar números em base hexadecimal você deve usar o prefixo 0x (e.g.,
0x300).
0f
HARDWARE                               PARÂMETRO A ESPECIFICAR07
IBM PS/1 ou ValuePoint (disco IDE)     0fhd=0bcylinders0f,0bheads0f,0bsectors07
Alguns ThinkPads IBM                   0ffloppy.floppy=thinkpad07
Protege regiões de portas de E/S       0freserve=0biobase0f,0bextent07[0f,0b...07]
Laptops com problemas de display       0fvga=77107
Usa primeira porta serial a 9600 baud  0fconsole=ttyS0,9600n807
Força o uso de um driver IDE genérico  0fgeneric.all_generic_ide=107

Possíveis (temporários) contornos para travamentos ou outras falhas de hardware:
desabilita roteamento de interrupções ACPI problemáticas  0fnoapic nolapic07
(parcialmente) desabilita ACPI                            0facpi=noirq07 or 0facpi=off07
desabilita USB                                            0fnousb07

Por exemplo:
  boot: install vga=771 noapic nolapic


Pressione F1control e F seguido de 1 para o índice de ajuda, ou ENTER para 
